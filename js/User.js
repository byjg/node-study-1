function User(n) {
    var paid = true; // private (to each User instance)

    this.name = n;     // public

    // Public
    this.setName = function (value) {
        this.name = value;
    };

    this.getPaid = function () {
        return paid;
    };

    // Public
    this.togglePaid = function togglePaid() {
        paid = !paid;
    };
}

module.exports = User;
