var User = require('../js/User');
var user = null;

beforeEach(() => {
    user = new User('Joao');
});

afterEach(() => {
    user = null;
});

test('Default Name by constructor', () => {
    expect(user.name).toBe('Joao');
});

test('Set name', () => {
    user.setName('Gilberto');
    expect(user.name).toBe('Gilberto');
});

test('Test Paid and GetToggle', () => {
    expect(user.getPaid()).toBeTruthy();
    user.togglePaid();
    expect(user.getPaid()).toBe(false);
    user.togglePaid();
    expect(user.getPaid()).toBe(true);
});

