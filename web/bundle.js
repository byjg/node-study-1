(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
function User(n) {
    var paid = true; // private (to each User instance)

    this.name = n;     // public

    // Public
    this.setName = function (value) {
        this.name = value;
    };

    // Public
    this.togglePaid = function togglePaid() {
        paid = !paid;
    };
}

module.exports = User;

},{}],2:[function(require,module,exports){
var User = require('./User');

var x = new User('bla');
var y = new User('ble');

console.log(x.name);
console.log(y.name);

x.setName('New name');

console.log(x.name);
console.log(y.name);



},{"./User":1}]},{},[2]);
