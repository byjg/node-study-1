# Meu teste com Node.js

## Preparar ambiente 

```
npm install -g browserify
```

## Create bundle

```
browserify js/main.js > web/bundle.js
```

I am using the article:

- https://www.keithcirkel.co.uk/why-we-should-stop-using-grunt/
- https://www.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/

So, i can:

```
npm run make-bundle
```